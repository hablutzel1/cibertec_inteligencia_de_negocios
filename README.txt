- data/ contiene las fuentes de datos, antes de ejecutar el proyecto de SSIS esta debe ser copiada a C:\.
- docs/ contiene el documento de MS Word.
- scripts/ contiene archivos SQL para crear la base de datos (script.sql incluye un SP para popular DimFecha).
- SSIS/ contiene el proyecto de SQL Server Integration Services, para la ejecuci�n de este primero se debe copiar la carpeta data/ como se menciona antes, crear la base de datos limpia usando script.sql y activar la autenticaci�n de Windows para MS SQL Server en "localhost" si esta no est� activada de antemano.
- temp/ contiene otros archivos que se deber�a revisar y borrar.

