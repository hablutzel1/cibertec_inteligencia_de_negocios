USE [master]
GO
/****** Object:  Database [DWHAgenciaViaje]    Script Date: 19/11/2016 3:10:07 p. m. ******/
CREATE DATABASE [DWHAgenciaViaje]

GO
USE [DWHAgenciaViaje]
GO
/****** Object:  Table [dbo].[DimAerolineas]    Script Date: 19/11/2016 3:10:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimAerolineas](
	[idAerolineas] [int] NOT NULL,
	[NombreAerolinea] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[idAerolineas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DimAvion]    Script Date: 19/11/2016 3:10:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimAvion](
	[IdAvion] [int] NOT NULL,
	[TipoAvion] [nvarchar](50) NULL,
	[Nombre] [nvarchar](50) NULL,
	[Placa] [nvarchar](50) NULL,
	[Capacidad] [int] NULL,
	[NroAsientos_PrimeraClase] [nvarchar](50) NULL,
	[NroAsientos_Turistico] [varchar](50) NULL,
	[NroAsientos_Empresarial] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdAvion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DimCliente]    Script Date: 19/11/2016 3:10:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DimCliente](
	[idCliente] [int] NOT NULL,
	[Nombre] [nvarchar](50) NULL,
	[Apellido] [nvarchar](50) NULL,
	[DNI] [int] NULL,
	[fechaNacimiento] [date] NULL,
	[Direccion] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[idCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DimDestino]    Script Date: 19/11/2016 3:10:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DimDestino](
	[IdDestino] [int] NOT NULL,
	[DireccionDestino] [nvarchar](50) NULL,
	[NombreDestino] [nvarchar](50) NULL,
	[PaisDestino] [nvarchar](50) NULL,
	[CuidadDestino] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDestino] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DimFecha]    Script Date: 19/11/2016 3:10:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DimFecha](
	[IdFecha] [int] NOT NULL,
	[Fecha] [datetime] NULL,
	[Anhio] [int] NULL,
	[Semestre] [varchar](20) NULL,
	[Trimestre] [varchar](20) NULL,
	[Mes] [varchar](20) NULL,
	[NumeroMes] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdFecha] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DimOrigen]    Script Date: 19/11/2016 3:10:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DimOrigen](
	[IdOrigen] [int] NOT NULL,
	[DireccionOrigen] [nvarchar](50) NULL,
	[NombreOrigen] [nvarchar](50) NULL,
	[PaisOrigen] [nvarchar](50) NULL,
	[CuidadOrigen] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdOrigen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DimPaqueteTuristico]    Script Date: 19/11/2016 3:10:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DimPaqueteTuristico](
	[IdPaqueteTuristico] [int] NOT NULL,
	[NombrePaquete] [nvarchar](50) NULL,
	[LugarTuristico] [nvarchar](50) NULL,
	[Nro_Personas] [int] NULL,
	[Nro_Dias] [int] NULL,
	[Nro_Noches] [int] NULL,
	[Estado] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPaqueteTuristico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DimTipoAsiento]    Script Date: 19/11/2016 3:10:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DimTipoAsiento](
	[IdTipoAsiento] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdTipoAsiento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FactPaquetesCancelados]    Script Date: 19/11/2016 3:10:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FactPaquetesCancelados](
	[IdCliente] [int] NULL,
	[IdPaqueteTuristico] [int] NULL,
	[IdFecha] [int] NULL,
	[FechaDeCancelacion] [date] NULL,
	[MotivoCancelacion] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FactVentaPaqueteTuristico]    Script Date: 19/11/2016 3:10:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FactVentaPaqueteTuristico](
	[IdPaqueteTuristico] [int] NULL,
	[IdFecha] [int] NULL,
	[idCliente] [int] NULL,
	[idAerolineas] [int] NULL,
	[IdTipoAsiento] [int] NULL,
	[IdAvion] [int] NULL,
	[IdOrigen] [int] NULL,
	[IdDestino] [int] NULL,
	[Precio] [decimal](18, 2) NULL,
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[FactPaquetesCancelados]  WITH CHECK ADD  CONSTRAINT [FK_FactPaquetesCancelados_DimCliente] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[DimCliente] ([idCliente])
GO
ALTER TABLE [dbo].[FactPaquetesCancelados] CHECK CONSTRAINT [FK_FactPaquetesCancelados_DimCliente]
GO
ALTER TABLE [dbo].[FactPaquetesCancelados]  WITH CHECK ADD  CONSTRAINT [FK_FactPaquetesCancelados_DimFecha] FOREIGN KEY([IdFecha])
REFERENCES [dbo].[DimFecha] ([IdFecha])
GO
ALTER TABLE [dbo].[FactPaquetesCancelados] CHECK CONSTRAINT [FK_FactPaquetesCancelados_DimFecha]
GO
ALTER TABLE [dbo].[FactPaquetesCancelados]  WITH CHECK ADD  CONSTRAINT [FK_FactPaquetesCancelados_DimPaqueteTuristico] FOREIGN KEY([IdPaqueteTuristico])
REFERENCES [dbo].[DimPaqueteTuristico] ([IdPaqueteTuristico])
GO
ALTER TABLE [dbo].[FactPaquetesCancelados] CHECK CONSTRAINT [FK_FactPaquetesCancelados_DimPaqueteTuristico]
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico]  WITH CHECK ADD  CONSTRAINT [FK_FactVentaPaqueteTuristico_DimAerolineas] FOREIGN KEY([idAerolineas])
REFERENCES [dbo].[DimAerolineas] ([idAerolineas])
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico] CHECK CONSTRAINT [FK_FactVentaPaqueteTuristico_DimAerolineas]
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico]  WITH CHECK ADD  CONSTRAINT [FK_FactVentaPaqueteTuristico_DimCliente] FOREIGN KEY([idCliente])
REFERENCES [dbo].[DimCliente] ([idCliente])
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico] CHECK CONSTRAINT [FK_FactVentaPaqueteTuristico_DimCliente]
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico]  WITH CHECK ADD  CONSTRAINT [FK_FactVentaPaqueteTuristico_DimDestino] FOREIGN KEY([IdDestino])
REFERENCES [dbo].[DimDestino] ([IdDestino])
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico] CHECK CONSTRAINT [FK_FactVentaPaqueteTuristico_DimDestino]
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico]  WITH CHECK ADD  CONSTRAINT [FK_FactVentaPaqueteTuristico_DimFecha] FOREIGN KEY([IdFecha])
REFERENCES [dbo].[DimFecha] ([IdFecha])
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico] CHECK CONSTRAINT [FK_FactVentaPaqueteTuristico_DimFecha]
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico]  WITH CHECK ADD  CONSTRAINT [FK_FactVentaPaqueteTuristico_DimOrigen] FOREIGN KEY([IdOrigen])
REFERENCES [dbo].[DimOrigen] ([IdOrigen])
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico] CHECK CONSTRAINT [FK_FactVentaPaqueteTuristico_DimOrigen]
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico]  WITH CHECK ADD  CONSTRAINT [FK_FactVentaPaqueteTuristico_DimPaqueteTuristico] FOREIGN KEY([IdPaqueteTuristico])
REFERENCES [dbo].[DimPaqueteTuristico] ([IdPaqueteTuristico])
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico] CHECK CONSTRAINT [FK_FactVentaPaqueteTuristico_DimPaqueteTuristico]
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico]  WITH CHECK ADD  CONSTRAINT [FK_FactVentaPaqueteTuristico_DimTipoAsiento] FOREIGN KEY([IdTipoAsiento])
REFERENCES [dbo].[DimTipoAsiento] ([IdTipoAsiento])
GO
ALTER TABLE [dbo].[FactVentaPaqueteTuristico] CHECK CONSTRAINT [FK_FactVentaPaqueteTuristico_DimTipoAsiento]
GO
/****** Object:  StoredProcedure [dbo].[sPopulateDimCalendar]    Script Date: 19/11/2016 3:10:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [dbo].[sPopularDimFecha]
 
@FromDate DATE = '2000-01-01',
@ToDate DATE = '2001-12-31'
 
AS
 
BEGIN
 
--Setting language
SET LANGUAGE English
 
--Populating Dimension
WHILE @FromDate <= @ToDate
	BEGIN
		INSERT INTO DimFecha(
			[IdFecha], 
			[Fecha], 			
			[NumeroMes], 
			[Anhio], 			
			[Mes], 	
			Trimestre,
			Semestre			
            )
            SELECT 
                YEAR(@FromDate)*10000 + MONTH(@FromDate) * 100 + DAY(@FromDate) AS DateKey, 
		@FromDate AS [Date],            
                MONTH(@FromDate) AS [Month],
                YEAR(@FromDate) AS [Year],	
                DATENAME(MONTH, @FromDate) AS MonthName,          
                DATENAME(QUARTER,@FromDate) AS [Quarter],
				CASE 
					WHEN DATENAME(QUARTER,@FromDate) = 1 OR DATENAME(QUARTER,@FromDate) = 2 THEN 1 
					ELSE 2
				END AS Semestre
             
      SET @FromDate = DATEADD(DAY, 1, @FromDate)
END
 
END


GO
USE [master]
GO
ALTER DATABASE [DWHAgenciaViaje] SET  READ_WRITE 
GO
